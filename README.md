[![training-dataset-dumper docs](https://img.shields.io/badge/info-documentation-informational)][tddd]

Flavor Tagging Ntuple Dumper
============================

This is to dump b-tagging info from an AnalysisBase release.

The [training-dataset-dumper documentation][tddd] is hosted via mkdocs. In case of issues with the hosting, or to access older documentation, see the `docs/` directory in this package.

Quickstart
----------

If you want to get the code up and running fast [without reading][s] the documentation:

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
source training-dataset-dumper/setup/analysisbase.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
cd ..
source build/x*/setup.sh

```

Run a test in `/tmp/` to see if everything worked:

```bash
test-dumper pflow
```

The first few lines printed to the terminal will tell you what code ran, and where. See the options in `test-dumper -h`. You can inspect the output files with

```bash
h5ls output.h5
```

Fede's notes
------------

## Quick run

Run a test in `/run/` (use `pflow` or `truth` options):
```bash
mkdir run
cd run
test-dumper -d ./ pflow
```

To run over a specified sample (in this case FTAG1 H->aa->cccc, ma=20GeV):

```bash
dump-single-btag -c ../training-dataset-dumper/configs/EMPFlow.json ../DAODs/mc20_13TeV/DAOD_FTAG1.34395165._000004.pool.root.1 --out-file outputn.h5
```

## Inspect output

Inspect in more detail the output

```bash
h5ls -v output.h5 
```

## Calibration 

Deactivate jet calibration by replacing in `configs/EMPFlow.json`:

```bash
"calibration": {
    "file": "fragments/pflow-calibration.json"
},
```

with

```bash
"calibration": {},
```

## Print content of h5 file (Setting up environment)

Set up the LCG view 101 which supports python3 and h5py.
```bash
source /cvmfs/sft.cern.ch/lcg/views/LCG_101/x86_64-centos7-clang12-opt/setup.sh
```
Save the following content as a python script called `print_jet_pt.py`.
```python
from h5py import File

input_file = "output_with_jet_calibration.h5"
input_file_raw = "output.h5"

with File(input_file, 'r') as h5file:
    jets = h5file['jets']
    print(jets['pt'])

with File(input_file_raw, 'r') as h5file_raw:
    jets_raw = h5file_raw['jets']
    print(jets_raw['pt'])
```
Execute the python script and compare the print-outs.
```bash
python3 print_jet_pt.py
```

## Jet variables

Add or remove jet level variable as pt, eta, E by editing ```configs/fragments/pflow-variables.json```


[tddd]: https://training-dataset-dumper.docs.cern.ch/
[s]: https://www.youtube.com/watch?v=wooGSr7k1-s

