#!/usr/bin/env python

"""

Bare bones flavor tagging info dumper

You should consider this the starting point for any more complicated
studies, all it does is set up the basic event loop and schedule the
dataset dumper.

"""

import sys
import json

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from BTagTrainingPreprocessing import dumper


def run():

    args = dumper.base_parser(__doc__).parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = getConfig(cfgFlags)
    ca.merge(PoolReadCfg(cfgFlags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    output = CompFactory.H5FileSvc(path=str(args.output))
    ca.addService(output)

    with open(args.config_file) as f:
        config_keys = json.load(f).keys()

    for cfg in config_keys:
        ca.addEventAlgo(CompFactory.SingleBTagAlg(
            name=f'{cfg}Dumper',
            output=output,
            configFileName=str(args.config_file),
            group=cfg,
        ))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
