#include "ConfigFileTools.hh"

#include <cmath> // NAN
#include <set>
#include <fstream>

#include <nlohmann/json.hpp>


namespace {
  // Merging lists is just appending them for now
  void merge_lists(
    nlohmann::ordered_json& local,
    const nlohmann::ordered_json& fragment) {
    if (!local.is_array() || !fragment.is_array()) {
      std::string err = "list merge error: "
        + local.dump() + " + " + fragment.dump();
      throw std::logic_error(err);
    }
    for (const auto& item: fragment) {
      local.push_back(item);
    }
  }

  // Merge a new tree into the existing one
  //
  void merge_trees(nlohmann::ordered_json& local,
                   const nlohmann::ordered_json& fragment) {

    // For primative types there's nothing to do: the local one takes
    // precedence over any fragment.

    // For lists, just append the new values.
    if (local.is_array()) {
      merge_lists(local, fragment);
      return;
    } else if (local.is_object()) {
      // Objects are slightly more complicated: if the fragment
      // contains anything that isn't in the local tree, we add
      // it. Otherwise we attempt to merge it.
      for (auto& [key, value]: fragment.items()) {
        // Add new entries, only if they don't currently exist
        if (not local.contains(key)) {
          local[key] = value;
        } else {
          // If both trees have this object, try to merge them
          merge_trees(local.at(key), value);
        }
      }
    }
  }
}


namespace ConfigFileTools {

  // Expand file fragments
  //
  void combine_files(nlohmann::ordered_json& local,
                     std::filesystem::path config_path) {
    namespace fs = std::filesystem;

    // magic key for file fragments
    const std::string fragment_key = "file";

    // Start by building the local tree recursively.
    for (auto& sub: local) {
      if (sub.is_structured()) combine_files(sub, config_path);
    }

    // if there's no fragment at this level we're done
    if (!local.count(fragment_key)) return;

    // If we find a file fragment, we expand it. We look in two
    // places: the path relative to the working directory takes
    // precedence, but we'll also look relative to the configuration
    // path.
    fs::path fragment_path(local.at(fragment_key));
    if (!fs::exists(fragment_path)) {
      fragment_path = config_path / fragment_path;
    }
    std::ifstream stream(fragment_path.string());
    nlohmann::ordered_json fragment = nlohmann::json::parse(stream);

    // Recursively fill out the fragment, in case it has its own
    // sub-fragments. The search path for sub-fragments should include
    // the directory where this fragment lives.
    combine_files(fragment, fragment_path.parent_path());

    // Now we merge the fragment into the local tree. The local tree
    // will take precedence.
    merge_trees(local, fragment);

    // We're done with the fragment, get rid of any reference to it.
    local.erase(fragment_key);
  }

  // class to check that all keys are used
  //
  OptionalConfigurationObject::OptionalConfigurationObject(
    nlohmann::ordered_json cfg):
    m_cfg(cfg)
  {
  }
  void OptionalConfigurationObject::throw_if_unused(
    const std::string& type)
  {
    if (m_cfg.empty()) return;
    std::string problem = "Found unknown " + type + " in configuration";
    std::string sep = ": ";
    for (const auto& el: m_cfg.items()) {
      problem.append(sep + el.key());
      sep = ", ";
    }
    throw std::runtime_error(problem);
  }
  // specialization for const char* type, string literals are weird in C++
  std::string OptionalConfigurationObject::get(
    const std::string& key,
    const char* default_value) {
    return get<std::string>(key, default_value);
  }
  bool OptionalConfigurationObject::empty() const {
    return m_cfg.empty();
  }


  bool boolinate(const nlohmann::ordered_json& nlocfg,
                         const std::string& key){

    std::string val = nlocfg.at(key);
    if (val == "true") return true;
    if (val == "false") return false;
    throw std::logic_error("'" + key + "' should be 'true' or 'false'");
  }
  bool boolinate_default(const nlohmann::ordered_json& nlocfg,
                         const std::string& key,
                         bool default_value){
    if (! nlocfg.contains(key) ) return default_value;
    return boolinate(nlocfg, key);
  }
  float null_as_nan(const nlohmann::ordered_json& node,
                    const std::string& key){
    const auto& child = node.at(key);
    if (child.is_null()) return NAN;
    return child.get<float>();
  }
  std::vector<std::string> get_list(const nlohmann::ordered_json& nlocfg){
    std::vector<std::string> vars;
    for(const auto& var: nlocfg.items()) {
      vars.push_back(var.value());
    }
    return vars;
  }

  std::set<std::string> get_key_set(const nlohmann::ordered_json& nlocfg){
    std::set<std::string> keys;
    for(const auto& pair: nlocfg.items()) {
      keys.insert(pair.key());
    }
    return keys;
  }

  void throw_if_any_keys_left(const std::set<std::string>& keys,
                              const std::string& type) {
    if (keys.empty()) return;
    std::string problem = "Found unknown " + type + " in configuration: ";
    for (const auto& k: keys) {
      problem.append(k);
      if (k != *keys.rbegin()) problem.append(", ");
    }
    throw std::runtime_error(problem);
  }
  MapOfLists get_variable_list(const nlohmann::ordered_json& nlocfg) {
    MapOfLists var_map;
    for(const auto& node: nlocfg.items()) {
      var_map[node.key()] = get_list(node.value());
    }
    return var_map;
  }


  std::map<std::string, std::string> check_map_from(
    const MapOfLists& default_to_vals){
    std::map<std::string, std::string> out;
    for (const auto& pair: default_to_vals) {
      const std::string& check_var = pair.first;
      for (const auto& var: pair.second) {
        if (out.count(var)) {
          throw std::logic_error(var + " already in map");
        }
        out[var] = check_var;
      }
    }
    return out;
  }

}
